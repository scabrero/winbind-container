#!/bin/bash -eu

BASENAME=$(basename "$1")

if [ "$BASENAME" = 'label-install' ] || [ "$BASENAME" = 'label-uninstall' ]; then
   exec "$@"
   exit 0
fi

exec "$@"
