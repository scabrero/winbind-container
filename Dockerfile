# SPDX-License-Identifier: MIT
# Defines the tag for OBS and build script builds:
#!BuildTag: %%TAGPREFIX%%/winbind:%%PKG_VERSION%%
#!BuildTag: %%TAGPREFIX%%/winbind:%%PKG_VERSION%%-%RELEASE%
#!BuildTag: %%TAGPREFIX%%/winbind:latest

FROM opensuse/tumbleweed:latest

LABEL maintainer="SUSE Labs Samba Team <samba-maintainers@suse.de>"

# Mandatory labels for the build service:
#   https://en.opensuse.org/Building_derived_containers
# labelprefix=%%LABELPREFIX%%
LABEL org.opencontainers.image.title="Winbind Container Image"
LABEL org.opencontainers.image.description="Winbind container based on Tumbleweed"
LABEL org.opencontainers.image.version="%%PKG_VERSION%%.%RELEASE%"
LABEL org.opencontainers.image.url="https://gitlab.suse.de/scabrero/winbind-container/"
LABEL org.opencontainers.image.created="%BUILDTIME%"
LABEL org.opensuse.reference="%%REGISTRY%%/%%TAGPREFIX%%/winbind:%%PKG_VERSION%%-%RELEASE%"
LABEL org.openbuildservice.disturl="%DISTURL%"
LABEL com.suse.supportlevel="techpreview"
LABEL com.suse.eula="beta"
LABEL com.suse.image-type="application"
LABEL com.suse.release-stage="prototype"
# endlabelprefix

RUN zypper --non-interactive addrepo --refresh --priority 25 \
    -r 'https://download.opensuse.org/repositories/home:/scabrero:/branches:/SUSE:/ALP:/Workloads/standard/home:scabrero:branches:SUSE:ALP:Workloads.repo'
RUN zypper --non-interactive --gpg-auto-import-keys refresh
RUN zypper --non-interactive install --no-recommends --no-confirm \
	patch \
	samba-winbind

RUN mkdir /container
COPY label-install \
     label-uninstall \
     label-join \
     nsswitch.conf.patch \
     winbind.service.in \
     /container/

RUN chmod +x /container/label-*

COPY entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["winbindd", "-F", "--no-process-group"]

LABEL INSTALL="podman run --rm --privileged --env IMAGE=IMAGE --pid host -v /:/host -v /run:/run:rslave IMAGE /container/label-install"
LABEL UNINSTALL="podman run --rm --privileged --env IMAGE=IMAGE --pid host -v /:/host -v /run:/run:rslave IMAGE /container/label-uninstall"
LABEL JOIN="podman run --rm --privileged --env IMAGE=IMAGE --pid host -v /:/host -v /run:/run:rslave -v /var/lib/samba:/var/lib/samba:z -v /var/log/samba:/var/log/samba:z -v /etc/samba:/etc/samba:z --net=host --tz local IMAGE /container/label-join"
